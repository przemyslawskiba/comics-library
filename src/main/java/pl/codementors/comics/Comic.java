package pl.codementors.comics;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Representation of single comics in library. Describes comics title, author, cover type, publish year and month.
 *
 * @author psysiu
 */
public class Comic implements Serializable {

    /**
     * Describes comics cover type.
     */
    public enum Cover {
        HARD,
        SOFT;
    }

    /**
     * Comic title.
     */
    private String title;

    /**
     * Comic author.
     */
    private String author;

    /**
     * Comics series.
     */
    private String series;

    /**
     * Publish year. Must be positive integer from 1867 (date of first serialized comics for a mass audience)
     * - 2017 range.
     */
    private int publishYear;

    /**
     * Publish month. Must be positive integer from 1 - 12 range.
     */
    private int publishMonth;

    /**
     * Comic cover.
     */
    private Cover cover;

    /**
     * Default constructor. Sets title and author to empty strings, publish year to 1867,
     * publish month to 1 and cover to soft.
     */
    public Comic() {

        setTitle("");
        setAuthor("");
        setPublishYear(1867);
        setPublishMonth(1);
        setCover(Cover.SOFT);
    }

    /**
     * Creates new comics. If publish year is not in range 1867 - 2017, the 1867 value is used. If publish month
     * is not in range 1 - 12, the 1 value is used.
     *
     * @param title        Comic title.
     * @param author       Comic author.
     * @param cover        Comic cover.
     * @param series       Comics series.
     * @param publishYear  Comic publish year (1867 - 2017).
     * @param publishMonth Comic publish month (1 - 12).
     */
    public Comic(String title, String author, String series, Cover cover, int publishYear, int publishMonth) {

        this.title = title;
        this.author = author;
        this.series = series;
        this.cover = cover;

        int year = Calendar.getInstance().get(Calendar.YEAR);

        if (publishYear >= 1867 && publishYear <= year) {
            this.publishYear = publishYear;
        } else {
            this.publishYear = 1867;
        }

        if (publishMonth >= 1 && publishMonth <= 12) {
            this.publishMonth = publishMonth;
        } else {
            this.publishMonth = 1;
        }

    }

    /**
     * @return Comic title.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * @param title New value for comics title.
     */
    public void setTitle(String title) {

        this.title = title;
    }

    /**
     * @return Comic publish year.
     */
    public int getPublishYear() {

        return this.publishYear;

    }

    /**
     * Sets new value for publish year. If new value is not in range 1867 - 2017 it does nothing.
     *
     * @param publishYear New value for comics publish year.
     */
    public void setPublishYear(int publishYear) {

        int year = Calendar.getInstance().get(Calendar.YEAR);

        if (publishYear >= 1867 && publishYear <= year)
        this.publishYear = publishYear;

    }

    /**
     * @return Comic publish month.
     */
    public int getPublishMonth() {

        return this.publishMonth;
    }

    /**
     * Sets new value for publish month. If new value is not in range 1 - 12 it does nothing.
     *
     * @param publishMonth New value for comics publish month.
     */
    public void setPublishMonth(int publishMonth) {

        if (publishMonth >= 1 && publishMonth <= 12)
        this.publishMonth = publishMonth;

    }

    /**
     * @return Comic author.
     */
    public String getAuthor() {

        return this.author;

    }

    /**
     * @param author New value for comics author.
     */
    public void setAuthor(String author) {

        this.author = author;

    }

    /**
     * @return Comic cover.
     */
    public Cover getCover() {

        return cover;

    }

    /**
     * @param cover New value for comics.
     */
    public void setCover(Cover cover) {

        this.cover = cover;

    }

    /**
     * @return Comic series.
     */
    public String getSeries() {

        return this.series;

    }

    /**
     * @param series New value for comic series.
     */
    public void setSeries(String series) {

        this.series = series;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comic comic = (Comic) o;

        if (publishYear != comic.publishYear) return false;
        if (publishMonth != comic.publishMonth) return false;
        if (!title.equals(comic.title)) return false;
        if (!author.equals(comic.author)) return false;
        if (!series.equals(comic.series)) return false;
        return cover == comic.cover;
    }

}
