package pl.codementors.comics;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();


    /**
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {

        return comics;
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {

        if (comic != null) {
            comics.add(comic);
        }
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {

        comics.remove(comic);

    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {

        for (Comic comic : comics) {
            comic.setCover(cover);
        }
    }


    /**
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {

        Collection<String> authors = new HashSet<>();
        for (Comic comic : comics) {
            authors.add(comic.getAuthor());
        }
        return authors;
    }


    /**
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {

        Collection<String> series = new HashSet<>();
        for (Comic comic : comics) {
            series.add(comic.getSeries());
        }
        return series;
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     * <p>
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     * <p>
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     * <p>
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {
        try (FileReader fr = new FileReader(file);
             Scanner scanner = new Scanner(fr)) {
            int numberOfComics = scanner.nextInt();
            scanner.skip("\n");
            for (int i = 0; i < numberOfComics; i++) {

                Comic comic = new Comic();
                String title = scanner.nextLine();
                String author = scanner.nextLine();
                String series = scanner.nextLine();
                String Cover = scanner.next();
                int publishMonth = scanner.nextInt();
                int publishYear = scanner.nextInt();
                scanner.skip("\n");

                comic.setTitle(title);
                comic.setAuthor(author);
                comic.setSeries(series);
                comic.setCover(Comic.Cover.valueOf(Cover));
                comic.setPublishMonth(publishMonth);
                comic.setPublishYear(publishYear);

                comics.add(comic);
            }

        } catch (RuntimeException | IOException ex) {
            System.out.println("dupa");
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {

        int i = 0;
        for (Comic comic : comics) {
            if (series.equals(comic.getSeries())) {
                i++;
            }
        }
        return i;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {

        int i = 0;
        for (Comic comic : comics) {
            if (author.equals(comic.getAuthor())) {
                i++;
            }
        }
        return i;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {

        int i = 0;
        for (Comic comic : comics) {
            if (year == comic.getPublishYear()) {
                i++;
            }
        }
        return i;
    }

    /**
     * Counts all comics with the same provided publish year and month.
     *
     * @param year  Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {

        int i = 0;
        for (Comic comic : comics) {
            if (year == comic.getPublishYear() && month == comic.getPublishMonth()) {
                i++;
            }
        }
        return i;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {

        Iterator iterator = comics.iterator();

        while (iterator.hasNext()) {
            Comic comic = (Comic) iterator.next();
            if (comic.getPublishYear() < year) {
                iterator.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {

        Iterator iterator = comics.iterator();

        while (iterator.hasNext()) {
            Comic comic = (Comic) iterator.next();
            if (comic.getAuthor() == author) {
                iterator.remove();
            }
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     * @param s
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {
        Map<String, Collection<Comic>> author = new HashMap<>();

        Iterator iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = (Comic) iterator.next();
            if (!author.containsKey(comic.getAuthor())) {
                HashSet<Comic> list = new HashSet<>();
                list.add(comic);
                author.put(comic.getAuthor(), list);
            }

            author.get(comic.getAuthor()).add(comic);
        }
        return author;
    }




    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {

        Map<Integer, Collection<Comic>> years = new HashMap<>();

        Iterator iterator = comics.iterator();

        while (iterator.hasNext()) {
            Comic comic = (Comic) iterator.next();
            if (!years.containsKey(comic.getPublishYear())) {
                HashSet<Comic> list = new HashSet<>();
                list.add(comic);
                years.put(comic.getPublishYear(), list);
            }

            years.get(comic.getPublishYear()).add(comic);
        }
        return years;
    }


    public void removeAllNewerThan(int year){

        Iterator iterator = comics.iterator();

        while (iterator.hasNext()) {

            Comic comic = (Comic) iterator.next();

            if (comic.getPublishYear() > year) {
                iterator.remove();
            }
        }
    }

    public void removeAllFromYearAndMonth(int year, int month) {


        Iterator iterator = comics.iterator();

        while (iterator.hasNext()) {
            Comic comic = (Comic) iterator.next();
            if (year == comic.getPublishYear() && month == comic.getPublishMonth()) {
                iterator.remove();
            }
        }
    }


    public Map<Pair<Integer, Integer>, Collection<Comic>> getYearsMonthsComics() {

        /*
        dla kazdego komiksu w bazie {
pobierz rok wydania;
pobierz miesiac wydania
jesli mapa nie zawiera pary rok i miesiac (klucza {
stworz pusta liste komiksow
przypisz liste do pary (data)
}
pobierz kolekcje komiksow z pary z mpay
dodaj do kolekcji aktualny komiks
         */

        Map<Pair<Integer, Integer>, Collection<Comic>> date = new HashMap<>();

        Iterator iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = (Comic) iterator.next();
            Pair pair = new ImmutablePair(comic.getPublishYear(), comic.getPublishMonth());
            if (!date.containsKey(pair)) {
                HashSet<Comic> list = new HashSet<>();
                list.add(comic);
                date.put(pair, list);
            }
            date.get(pair).add(comic);
        }
        return date;
    }
}



